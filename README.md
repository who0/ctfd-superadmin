# core-beta based + theme

Rewritten version of the CTFd core theme to use Bootstrap 5, Alpine.js, and vite to improve upon the existing CTFd theme structure. 

## Custom super-admin theme
- add collapse mode with alpine.js
- add knamiCode javascript
- add space-invaders peice of code.
- add fr template

## Todo

- Document how we are using Vite
- Create a cookie cutter template package to use with Vite
