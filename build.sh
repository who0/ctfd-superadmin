
if [ ! -e /.dockerenv ] 
then
docker run -it -v $PWD:/opt alpine  sh -c /opt/build.sh

else
#
cd /opt
apk add npm
npm install vite
npm run build

fi
